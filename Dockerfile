FROM node:current

ADD dist /app

ENTRYPOINT node /app/app.js